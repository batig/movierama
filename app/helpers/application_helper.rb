module ApplicationHelper
  # Setup sorting links and alternate between ASC and DESC.
  def sortable(column, title = nil)
    title ||= column.titleize
    direction = column == sort_column && sort_direction == 'asc' ? 'desc' : 'asc'
    link_to title, { sort: column, direction: direction },
    { class: 'btn btn-default active', role: 'button' }
  end

  def time_passed(since)
    time_ago_in_words since
  end
end
