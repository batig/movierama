class AddUpvotesAndDownvotesToMovie < ActiveRecord::Migration
  def change
    add_column :movies, :upvotes, :integer
    add_column :movies, :downvotes, :integer
  end
end
