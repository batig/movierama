class MoviesController < ApplicationController

  skip_before_filter :require_login, :only => [:index, :show]

  def index
    @movies = Movie.order(sort_column + ' ' + sort_direction)
    @posters = @movies.map { |m| User.find(m.user_id) }
  end

  def show
    @movie = Movie.find(params[:id])
    @poster = User.find_by_id(@movie.user_id).username
  end

  def new
    @movie = Movie.new
  end

  def create
    @movie = current_user.movies.build(movie_params)
    @movie.user_id = current_user.id
    @movie.save

    redirect_to @movie
  end

  def upvote
    @movie = Movie.find(params[:movie_id])
    @movie.increment!(:upvotes)
    vote = @movie.votes.find_or_create_by(
      user_id: current_user.id,
      movie_id: params[:movie_id])
    vote.up!
    vote.save

    redirect_to movies_path
  end

  def downvote
    @movie = Movie.find(params[:movie_id])
    @movie.increment!(:downvotes)
    vote = @movie.votes.find_or_create_by(
      user_id: current_user.id,
      movie_id: params[:movie_id])
    vote.down!
    vote.save

    redirect_to movies_path
  end

  def changevote
    @movie = Movie.find(params[:movie_id])
    vote = Vote.find_by_movie_id_and_user_id(params[:movie_id], current_user.id)
    @movie.decrement!(vote.category + 'votes')
    Vote.destroy(vote.id)

    if params[:category] == 'up'
      upvote
    else # Down
      downvote
    end
  end

  def retract
    @movie = Movie.find(params[:movie_id])
    vote = Vote.find_by_movie_id_and_user_id(params[:movie_id], current_user.id)
    @movie.decrement!(vote.category + 'votes')
    Vote.destroy(vote.id)

    redirect_to movies_path
  end

  private

  def movie_params
    params.require(:movie).permit(:title, :description, :upvotes, :downvotes)
  end
end
