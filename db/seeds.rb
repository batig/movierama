# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

User.create!(:email => "alice@example.com", :username => "alice", :password => "alicealice", :password_confirmation => "alicealice")
User.create!(:email => "bob@example.com", :username => "bob", :password => "bobbob", :password_confirmation => "bobbob")
User.create!(:email => "eve@example.com", :username => "eve", :password => "eveeve", :password_confirmation => "eveeve")


Movie.create!(title: '2001: A Space Odyssey', user_id: 1, upvotes: 2, downvotes: 0, created_on: "2015-10-13 18:51:31", description: "Humanity finds a mysterious, obviously artificial object buried beneath the Lunar surface and, with the intelligent computer H.A.L. 9000, sets off on a quest.")
Movie.create!(title: 'A Clockwork Orange', user_id: 1, upvotes: 1, downvotes: 1, created_on: "2015-10-13 17:51:31", description: "In future Britain, charismatic delinquent Alex DeLarge is jailed and volunteers for an experimental aversion therapy developed by the government in an effort to solve society's crime problem - but not all goes according to plan.")
Movie.create!(title: 'The Life Aquatic with Steve Zissou', user_id: 2, upvotes: 0, downvotes: 1, created_on: "2015-10-12 13:51:31", description: "With a plan to exact revenge on a mythical shark that killed his partner, oceanographer Steve Zissou rallies a crew that includes his estranged wife, a journalist, and a man who may or may not be his son.")
Movie.create!(title: 'The Lion King', user_id: 2, upvotes: 1, downvotes: 0, created_on: "2015-10-14 17:51:31", description: "Lion cub and future king Simba searches for his identity. His eagerness to please others and penchant for testing his boundaries sometimes gets him into trouble.")
Movie.create!(title: 'Frank', user_id: 3, upvotes: 0, downvotes: 0, created_on: "2015-10-14 17:51:31", description: "Jon, a young wanna-be musician, discovers he's bitten off more than he can chew when he joins an eccentric pop band led by the mysterious and enigmatic Frank.")


Vote.create!(:user_id => 1, :movie_id => 3, :category => 0)
Vote.create!(:user_id => 1, :movie_id => 4, :category => 1)
Vote.create!(:user_id => 2, :movie_id => 1, :category => 1)
Vote.create!(:user_id => 2, :movie_id => 2, :category => 1)
Vote.create!(:user_id => 3, :movie_id => 1, :category => 1)
Vote.create!(:user_id => 3, :movie_id => 2, :category => 0)
