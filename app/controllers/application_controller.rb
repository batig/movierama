class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :require_login

  private

  # Minimal sessions and authorization implementations.
  def current_user
    current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def require_login
    unless current_user
      redirect_to login_path, notice: 'Log in to get access!'
    end
  end

  # Set default sorting column and direction.
  def sort_column
    Movie.column_names.include?(params[:sort]) ? params[:sort] : 'title'
  end
  helper_method :sort_column

  def sort_direction
    %w(asc desc).include?(params[:direction]) ? params[:direction] : 'asc'
  end
  helper_method :sort_direction
end
