class User < ActiveRecord::Base
  has_many :movies, dependent: :destroy
  has_many :votes, through: :movies

  has_secure_password

  validates :email, :username, presence: true
  validates :email, :username, uniqueness: true
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: 'Doesn’t look valid.'
  validates :username, length: { minimum: 2 }
  validates :password, length: { in: 6..20 }
end
