class Movie < ActiveRecord::Base
  belongs_to :user
  has_many :votes

  validates :title, :description, presence: true
end
