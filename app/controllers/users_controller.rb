class UsersController < ApplicationController

  skip_before_filter :require_login, only: [:new, :create]

  def show
    @username = User.find(params[:id]).username.titleize
    @movies = Movie.where(user_id: params[:id]).order(
      sort_column + ' ' + sort_direction)
  end

  def new
    @user = User.new
  end

  # Sign up and auto-login
  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_to root_url, notice: 'Thanks for signing up!'
    else
      flash[:error] = 'Something went wrong. Try again!'
      render 'new'
    end
  end

  private

  def user_params
    params.require(:user).permit(
      :user_id,
      :username,
      :email,
      :password,
      :password_confirmation)
  end
end
