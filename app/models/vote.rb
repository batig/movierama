class Vote < ActiveRecord::Base
  belongs_to :user
  belongs_to :movie

  enum category: { down: 0, up: 1 }

  validates_uniqueness_of :movie_id, scope: :user_id
end
