module MoviesHelper
  def poster(movie)
    User.find_by_id(movie.user_id).username
  end

  def upvoted?(movie_id)
    !Vote.find_by_movie_id_and_user_id_and_category(movie_id, current_user.id, 1).nil?
  end

  def downvoted?(movie_id)
    !Vote.find_by_movie_id_and_user_id_and_category(movie_id, current_user.id, 0).nil?
  end
end
