Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.

  root :to => "movies#index"

  get "upvote" => "movies#upvote", :as => "upvote"
  get "downvote" => "movies#downvote", :as => "downvote"
  get "retract" => "movies#retract", :as => "retract"
  get "changevote" => "movies#changevote", :as => "changevote"

  get "login" => "sessions#new", :as => "login"
  get "signup" => "users#new", :as => "signup"
  get "logout" => "sessions#destroy", :as => "logout"


  resources :users, only: [:show, :new, :create]
  resources :sessions, only: [:create, :destroy]
  resources :movies, only: [:index, :show, :new, :create]




end
