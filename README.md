# MovieRama #

A simple demo web application where users submit and judge movies.

### Notes ###

Starting out, partly due to Giannis' remark regarding acts_as_votable, I took the decision to avoid using well known gems like Devise, CanCan and well, acts_as_votable, that would have made the development faster,easier and more robust. 

I believe the implemented app covers the requirements but could have been much better especially regarding testing and code quality. Partly due to being a bit rusty (much more than I expected) and having a busy week, I didn't manage to find the time to use AJAX and more importantly once more I managed to avoid studying about and implementing some tests!

Anyway, here it is. It was implemented using:

* Ruby: 2.0.0

* Rails: 4.2.4

To run locally just download the repo and run

```
#!ruby

bundle install
rake db:migrate
rake db:seed
rails s
```